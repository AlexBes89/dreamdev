<?php
/**
 * Template Name: My work
 */
get_header();

$text = get_field('text');
$link = get_field('link');
?>

<?php while ( have_posts() ) : the_post(); ?>

    <section class="mb-5">
        <div class="container">
            <?php print_html('<h2 class="mb-3">%1$s</h2>', $text); ?>
            <?php print_html('<a href="%1$s" class="btn btn-primary mb-3">%2$s</a>', [ $link['url'], $link['title'] ]); ?>

            <?php
            if( have_rows('repeater') ): ?>
                <ul>
                    <?php while( have_rows('repeater') ) : the_row(); ?>
                        <li><?php echo get_sub_field('text'); ?></li>  
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>
    </section>

    <section>
        <div class="container">
            latest news
        </div>
    </section>

<?php endwhile; ?>
<?php get_footer(); ?>