<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );
function theme_enqueue_scripts() {
	wp_enqueue_style('app.css', get_template_directory_uri() . '/css/app.css');
	wp_enqueue_script('app.js', get_template_directory_uri() . '/js/app.js', array(), false, true);
}

function print_html($tmpl, $values, $required = 0, $echo = true) {
    if (!empty($values[$required])) {

        $html = vsprintf($tmpl, $values);

        if ($echo) {
            echo $html;
        } else {
            return $html;
        }
    }
}

show_admin_bar( false );

add_action( 'init', 'register_menus' );
function register_menus() {
	register_nav_menus(
		array(
			'main-nav' => 'Footer menu 1',
			'main-nav' => 'Footer menu 2',
			'main-nav' => 'Footer menu 3',
		)
	);
}

register_sidebar(
	array(
		'name'          => esc_html__( 'Footer', 'dreamdev' ),
		'id'            => 'footer_widget',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	)
);

if( function_exists('acf_add_options_page') ) {
	acf_add_options_sub_page(array(
		'title' => 'Site Options',
		'parent' => 'options-general.php',
		'capability' => 'manage_options'
	));
	acf_add_options_sub_page('Test');
}

// cpt
